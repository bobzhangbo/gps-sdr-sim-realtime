#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>

#define BUFSIZE 1024

int sockinit(short port)
{
    int sock = socket(AF_INET,SOCK_STREAM, 0);
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);  
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");  
    if (connect(sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("connect");
        exit(1);
    }


    return sock;
}
void sockclose(int s){
	close(s);
}
int socksend(int s,void *dataa,int siz){
	return send(s,dataa,siz,0);
}

long int timem(){
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec*1000+t.tv_usec/1000;
}

int udpinit(short port){
    //printf("[udpinit] %d\n", port);
    int sock = socket(AF_INET,SOCK_DGRAM, 0);
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);  
    if (bind(sock, (struct sockaddr *)&servaddr, sizeof(servaddr))<0){
       perror("connect");
       exit(1);
    }
    return sock;
}

char buf[BUFSIZE]; /* message buf */

int udprecv(int s, void *buf, int siz)
{
    int ret = recvfrom(s, buf, siz, 0, NULL, NULL);
    if (ret < 0)
        perror("\nERROR in recvfrom");
    return ret;
}

double llhr[3]={39.68, 139.76, 10};

void threadrecv(){
	printf("[threadrecv] listening\n");
	short p=5678;
    int counter = 0;

    int s=udpinit(p);
    while(1) {
        //int output = udprecv(s, llhr, 3 * sizeof(double));
        udprecv(s, llhr, 3 * sizeof(double));
        if (counter < 100000) {
            counter++;
        } else {
            printf("\t%lf, %lf, %lf\n",llhr[0],llhr[1],llhr[2]);
//            printf("%d\n", output);
            counter = 0;
        }
    }
}

void threadudprecv(void *arguments){
    arg_struct_t *args = arguments;
    printf("listening on port %d\n", args->udpport);
    int s = udpinit(args->udpport);
    int ret;
    char identifier[10];
    double t, lat, lon, hgt;
    double *llh = (double *) args->llh;

    while(1){
        ret = udprecv(s, buf, BUFSIZE);
        if (ret < 0)
            perror("\nERROR in recvfrom");
        else
        {
            sscanf(buf, "%s %lf %lf %lf %lf", identifier, &t, &lat, &lon, &hgt);
            llh[0] = lat;
            llh[1] = lon;
            llh[2] = hgt;
            printf("\n[Rcvd: %s] lat=%f, lon=%f, hgt=%f\n", identifier, lat, lon, hgt);
        }
    }
}
		 
