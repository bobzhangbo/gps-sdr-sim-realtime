import socket
import time

UDP_IP = "127.0.0.1"
# UDP_PORT = 5007
UDP_PORT = 2234


lat = 47.653700
lon = -122.306306
hgt = 10.0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

count = 0
step = 0.000005

while True:
    try:
        loc = "0, {}, {}, {}".format(lat+count*step, lon, hgt)
        count += 1
        ret = sock.sendto(bytes(loc, "utf-8"), (UDP_IP, UDP_PORT))
        print("\tNext loc = ({}), ret value = {}".format(loc, ret))
        time.sleep(1)
    except KeyboardInterrupt:
        sock.close()
        break
