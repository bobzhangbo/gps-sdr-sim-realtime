# This script specifies global configurations for experiments and demos.

MODE_DEFAULT = 'default'
MODE_TEST = 'test'

MODE = MODE_DEFAULT
# MODE = MODE_TEST

if MODE == MODE_DEFAULT:
    SPOOFER_CONSOLE_ADDR = ('192.168.1.115', 5020)      # Address of spoofer console
    SPOOFER_ENGINE_ADDR = ('192.168.1.115', 2234)       # Address of spoofer's engine that generates I/Q data
    SPOOFER_USRP_ADDR = ('192.168.1.115', 1234)         # Address of spoofer's USRP
    SIMULATOR_MAPPING_ADDR = ('192.168.1.115', 5050)    # Address for real-time map plot
    SPOOFER_USRP_MAIN_ADDR = ('192.168.1.115', 5015)    # Address for usrp_combined.py
    SIMULATOR_ENGINE_ADDR = ('192.168.1.115', 2235)     # authentic
    SIMULATOR_USRP_ADDR = ('192.168.1.115', 1235)

elif MODE == MODE_TEST:
    SPOOFER_CONSOLE_ADDR = ('127.0.0.1', 5020)  # Address of spoofer console
    SPOOFER_ENGINE_ADDR = ('127.0.0.1', 2234)  # Address of spoofer's engine that generates I/Q data
    SPOOFER_USRP_ADDR = ('127.0.0.1', 1234)  # Address of spoofer's USRP
    SIMULATOR_MAPPING_ADDR = ('192.168.1.115', 5050)    # Address for real-time map plot
    SPOOFER_USRP_MAIN_ADDR = ('192.168.1.115', 5015)    # Address for usrp_combined.py

    SIMULATOR_ENGINE_ADDR = ('127.0.0.1', 2235)
    SIMULATOR_USRP_ADDR = ('127.0.0.1', 1235)

STATE_STANDBY = 'STANDBY'
STATE_TAKEOVER = 'TAKEOVER'
STATE_SPOOF = 'SPOOF'
STATE_DEVIATE = 'DEVIATE'

SPEED_KM_PER_SEC = 0.308667   # high-speed (600knots)
# SPEED_KM_PER_SEC = 0          # static case
# SPEED_KM_PER_SEC = 0.005        # low-speed

DEVIATION_VAL = 0.0000002
