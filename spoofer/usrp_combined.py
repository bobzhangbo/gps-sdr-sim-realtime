#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tcp Beta
# Generated: Mon Aug 13 13:06:03 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import threading
import time
import socket



data = {
    'ts': [],
    'lat': [],
    'lon': [],
    'hgt': []
}

# usrp_ip = '127.0.0.1'
# usrp_ip = '192.168.2.1'
usrp_ip = '192.168.1.115'
usrp_port = 5015


tcp_port_spoof = 1234   # spoof_signal_gen port: 2234
tcp_port_auth = 1235    # auth_signal_gen port: 2235

gps_sdr_sim_ip = '127.0.0.1'
gps_sdr_sim_port = 5010

AUTH_X_MAX = 10.0
SPOOF_X_MAX = 10.0
NOISE_X_MAX = 100000000.0
AMP_MAX = 100.0


class tcp_beta(gr.top_block):

    def __init__(self):
        # type: () -> object
        gr.top_block.__init__(self, "Tcp Beta")

        # ----- VARIABLES ---------------------------
        self.samp_rate = samp_rate = 2500000
        self.frequency = frequency = 1575420000
        self.g_spoof = g_spoof = 0
        self.g_auth = g_auth = 0
        self.m_auth = m_auth = 1
        self.m_spoof = m_spoof = 0
        self.m_noise = m_noise = 0
        self.noise_amp = noise_amp = 10
        self.delay_sec_spoof = delay_sec_spoof = 10
        self.delay_sec_auth = delay_sec_auth = 0
        self.delay_divider = 1


        # ----- BLOCKS ---------------------------
        self.uhd_usrp_sink_spoof = uhd.usrp_sink(
        	",".join(("serial=3136D6B", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )

        self.uhd_usrp_sink_auth = uhd.usrp_sink(
        	",".join(("serial=3150311", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )

        self.uhd_usrp_sink_spoof.set_clock_source('internal', 0)
        self.uhd_usrp_sink_spoof.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_spoof.set_center_freq(frequency, 0)
        self.uhd_usrp_sink_spoof.set_gain(g_spoof, 0)
        self.uhd_usrp_sink_spoof.set_antenna('TX/RX', 0)

        self.uhd_usrp_sink_auth.set_clock_source('internal', 0)
        self.uhd_usrp_sink_auth.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_auth.set_center_freq(frequency, 0)
        self.uhd_usrp_sink_auth.set_gain(g_auth, 0)
        self.uhd_usrp_sink_auth.set_antenna('TX/RX', 0)


        self.tcp_source_spoof = grc_blks2.tcp_source(
        	itemsize=gr.sizeof_short*1,
        	addr='127.0.0.1',
        	port=tcp_port_spoof,
        	server=True,
        )

        self.tcp_source_auth = grc_blks2.tcp_source(
        	itemsize=gr.sizeof_short*1,
        	addr='127.0.0.1',
        	port=tcp_port_auth,
        	server=True,
        )

        self.tcp_source_spoof.set_max_output_buffer(20000)
        self.tcp_source_auth.set_max_output_buffer(20000)

        self.multiply_const_vxx_spoof = blocks.multiply_const_vcc((0.001, ))
        self.multiply_const_vxx_auth = blocks.multiply_const_vcc((0.001, ))

        self.ishort_to_complex_spoof = blocks.interleaved_short_to_complex(False, False)
        self.ishort_to_complex_auth = blocks.interleaved_short_to_complex(False, False)

        self.multiply_xx_noise = blocks.multiply_vff(1)
        self.multiply_xx_spoof = blocks.multiply_vff(1)
        self.multiply_xx_auth = blocks.multiply_vff(1)

        self.analog_const_source_auth = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, m_auth)
        self.analog_const_source_spoof = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, m_spoof)
        self.analog_const_source_noise = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, m_noise)
        self.analog_noise_source = analog.noise_source_f(analog.GR_GAUSSIAN, noise_amp, 0)

        self.short_to_float_spoof = blocks.short_to_float(1, 1)
        self.short_to_float_auth = blocks.short_to_float(1, 1)
        self.float_to_short_spoof = blocks.float_to_short(1, 1)
        self.float_to_short_auth = blocks.float_to_short(1, 1)
        self.float_to_short_noise = blocks.float_to_short(1, 1)
        self.add_xx_spoof = blocks.add_vss(1)

        self.delay_spoof = blocks.delay(gr.sizeof_gr_complex*1, delay_sec_spoof*(samp_rate/self.delay_divider))
        self.delay_auth = blocks.delay(gr.sizeof_gr_complex*1, delay_sec_auth*samp_rate)

        # ----- CONNECTIONS ---------------------------
        self.connect((self.tcp_source_auth, 0), (self.short_to_float_auth, 0))
        self.connect((self.short_to_float_auth, 0), (self.multiply_xx_auth, 0))
        self.connect((self.analog_const_source_auth, 0), (self.multiply_xx_auth, 1))
        self.connect((self.multiply_xx_auth, 0), (self.float_to_short_auth, 0))
        self.connect((self.float_to_short_auth, 0), (self.ishort_to_complex_auth, 0))
        self.connect((self.ishort_to_complex_auth, 0), (self.multiply_const_vxx_auth, 0))
        # self.connect((self.multiply_const_vxx_auth, 0), (self.uhd_usrp_sink_auth, 0))

        self.connect((self.multiply_const_vxx_auth, 0), (self.delay_auth, 0))
        self.connect((self.delay_auth, 0), (self.uhd_usrp_sink_auth, 0))


        self.connect((self.tcp_source_spoof, 0), (self.short_to_float_spoof, 0))
        self.connect((self.short_to_float_spoof, 0), (self.multiply_xx_spoof, 0))
        self.connect((self.analog_const_source_spoof, 0), (self.multiply_xx_spoof, 1))
        self.connect((self.multiply_xx_spoof, 0), (self.float_to_short_spoof, 0))
        self.connect((self.float_to_short_spoof, 0), (self.add_xx_spoof, 0))
        self.connect((self.analog_noise_source, 0), (self.multiply_xx_noise, 0))
        self.connect((self.analog_const_source_noise, 0), (self.multiply_xx_noise, 1))
        self.connect((self.multiply_xx_noise, 0), (self.float_to_short_noise, 0))
        self.connect((self.float_to_short_noise, 0), (self.add_xx_spoof, 1))
        self.connect((self.add_xx_spoof, 0), (self.ishort_to_complex_spoof, 0))
        self.connect((self.ishort_to_complex_spoof, 0), (self.multiply_const_vxx_spoof, 0))

        self.connect((self.multiply_const_vxx_spoof, 0), (self.delay_spoof, 0))
        self.connect((self.delay_spoof, 0), (self.uhd_usrp_sink_spoof, 0))



    # --- SPOOFING SIGNAL DELAY --------------------------------
    def get_delay_sec_spoof(self):
        return self.delay_sec_spoof

    def set_delay_sec_spoof(self, delay_sec_spoof):
        self.delay_sec_spoof = delay_sec_spoof
        self.delay_spoof.set_dly(self.delay_sec_spoof*(self.samp_rate/self.delay_divider))


    # --- AUTHENTIC SIGNAL DELAY --------------------------------
    def get_delay_sec_auth(self):
        return self.delay_sec_auth

    def set_delay_sec_auth(self, delay_sec_auth):
        self.delay_sec_auth = delay_sec_auth
        self.delay_auth.set_dly(self.delay_sec_auth*self.samp_rate)



    # --- SAMPLE RATE --------------------------------
    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_spoof.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_auth.set_samp_rate(self.samp_rate)
        self.delay_auth.set_dly(self.delay_sec_auth*self.samp_rate)
        self.delay_spoof.set_dly(self.delay_sec_spoof*(self.samp_rate/self.delay_divider))


    # --- CENTER FREQUENCY --------------------------------
    def get_frequency(self):
        return self.frequency

    def set_frequency(self, frequency):
        self.frequency = frequency
        self.uhd_usrp_sink_spoof.set_center_freq(self.frequency, 0)
        self.uhd_usrp_sink_auth.set_center_freq(self.frequency, 0)


    # --- NOISE MULTIPLIER --------------------------------
    def get_m_noise(self):
        return self.m_noise

    def set_m_noise(self, m_noise):
        self.m_noise = m_noise
        self.analog_const_source_noise.set_offset(self.m_noise)


    # --- SPOOFING MULTIPLIER --------------------------------
    def get_m_spoof(self):
        return self.m_spoof

    def set_m_spoof(self, m_spoof):
        self.m_spoof = m_spoof
        self.analog_const_source_spoof.set_offset(int(self.m_spoof))


    # --- AUTHENTIC MULTIPLIER --------------------------------
    def get_m_auth(self):
        return self.m_auth

    def set_m_auth(self, m_auth):
        self.m_auth = m_auth
        self.analog_const_source_auth.set_offset(int(self.m_auth))


    # --- NOISE AMPLITUDE --------------------------------
    def set_noise_amp(self, noise_amp):
        self.noise_amp = noise_amp
        self.analog_noise_source.set_amplitude(noise_amp)

    def get_noise_amp(self):
        return self.noise_amp


    # --- SPOOFING GAIN --------------------------------
    def get_g_spoof(self):
        return self.g_spoof

    def set_g_spoof(self, g_spoof):
        self.g_spoof = g_spoof
        self.uhd_usrp_sink_spoof.set_gain(self.g_spoof, 0)


    # --- AUTHENTIC GAIN --------------------------------
    def get_g_auth(self):
        return self.g_auth

    def set_g_auth(self, g_auth):
        self.g_auth = g_auth
        self.uhd_usrp_sink_auth.set_gain(self.g_auth, 0)




def adjust_m_spoof(tb, value, elapsed_time):
    if value >= 1:
        tb.set_m_spoof(1)
        print("{}: spoofing signal = 1 (ON)".format(elapsed_time))
    else:
        tb.set_m_spoof(0)
        print("{}: spoofing signal = 0 (OFF)".format(elapsed_time))


def adjust_m_noise(tb, value, elapsed_time):
    if value <= NOISE_X_MAX:
        tb.set_m_noise(value)
    else:
        print("*** WARNING: Input value exceeds noise limit (MAX = {})".format(NOISE_X_MAX))
    print("{}: noise multiplier = {}".format(elapsed_time, tb.get_m_noise()))


def adjust_noise_amp(tb, value, elapsed_time):
    if value <= AMP_MAX:
        tb.set_noise_amp(value)
    else:
        print("*** WARNING: Input value exceeds amplitude limit (MAX = {})".format(AMP_MAX))
    print("{}: noise amplitude = {}".format(elapsed_time, tb.get_noise_amp()))


def adjust_g_spoof(tb, value, elapsed_time):
    tb.set_g_spoof(value)
    print("{}: hardware gain = {}".format(elapsed_time, tb.get_g_spoof()))


def string_state(sig):
    if sig == 1:
        return '1 (ON)'
    else:
        return '0 (OFF)'


def get_status(tb):
    gain = tb.get_g_spoof()
    sig = string_state(tb.get_m_spoof())
    noise = string_state(tb.get_m_noise())
    # amp = string_state(tb.get_noise_amp())

    print("-----------------------------------")
    print("hardware gain    (g) = {}".format(gain))
    print("spoofing signal  (s) = {}".format(sig))
    print("noise multiplier (n) = {}".format(noise))
    print("-----------------------------------\n")


def main(top_block_cls=tcp_beta, options=None):

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((usrp_ip, usrp_port))

    tb = top_block_cls()
    tb.start()

    get_status(tb)

    while True:

        try:
            msg, addr = sock.recvfrom(100)
            split_msg = msg.split(',')
            target = split_msg[0]
            value = float(split_msg[1])
            elapsed_time = float(split_msg[2])
            print(split_msg)

            if target == "s":
                adjust_m_spoof(tb, value, elapsed_time)
            if target == "n":
                adjust_m_noise(tb, value, elapsed_time)
            if target == "a":
                adjust_noise_amp(tb, value, elapsed_time)
            if target == "g":
                adjust_g_spoof(tb, value, elapsed_time)
            if target == "ls":
                get_status(tb)


        except KeyboardInterrupt:
            break

    sock.close()
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
