#!/usr/bin/env bash

function finish {
    echo ""
    echo "PROCESS TERMINATED"
    kill 0
}

trap finish EXIT

cd /Users/joannamazer/git/gitlab/gps-sdr-sim-realtime/spoofer
/opt/local/bin/python2.7 usrp_combined.py &

wait