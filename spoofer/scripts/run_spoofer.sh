#!/usr/bin/env bash
trap "kill 0" EXIT

cd /Users/joannamazer/git/gitlab/gps-sdr-sim-realtime
echo "--- SPOOFING SIGNAL GENERATOR ---"

# DYNAMIC CASE
#./gps-sdr-sim -e spoofer/ephemeris/brdc3540.14n -n 1234 -m 2234 -s 2500000 -d 1200 &

# STATIC CASE
./gps-sdr-sim -e spoofer/ephemeris/brdc3540.14n -n 1234 -s 2500000 -d 1200 -l 47.653700,-122.306306,10.0 &

while true; do
read -rsn1 input
if [ "$input" = "a" ]; then
    python3 spoofer/archive/spoof_signal_gen.py &
elif [ "$input" = "z" ]; then
    kill $!
fi
done

wait