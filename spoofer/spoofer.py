#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import threading
import socketserver
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.console
import time
from utility import *
from config import *

DEBUG = True

x_data = {'ref_gps_rx':[], 'spoofed':[], 'authentic':[]}
y_data = {'ref_gps_rx':[], 'spoofed':[], 'authentic':[]}
curves = {'ref_gps_rx':None, 'spoofed':None, 'authentic':None}


def add_data_point(label, lat, lon):
    if True: #label != 'spoofed':
        if lat > 0:
            try:
                x_data[label].append(lon)
                y_data[label].append(lat)
            except KeyError:
                print("Key {} not found".format(label))
        print("{}, lat: {}, lon: {} [NOT PLOTTING BECAUSE NO-FIX YET]".format(label, lat, lon))

def refresh(x, y, c):
    for label in x.keys():
        c[label].setData(x[label], y[label])

def display_console(spoofer):
    console_name = 'GPS Spoofing Console'
    console_text = """
                'a'  : Attack - Start spoofing attack (jam-and-spoof)
                'n'  : Normal - Launching stealthy spoofing attack (setting noise = 0, spoofing gain = 0)
                'd'  : Deviate - Start deviate the trajectory
                'r'  : Reset - Setting signal parameters back to default
                'ls' : List all parameters

                Enter a command:
                """

    app = QtGui.QApplication([])
    mw = QtGui.QMainWindow()
    mw.setWindowTitle(console_name)
    cw = QtGui.QWidget()
    mw.resize(800, 1200)
    mw.setCentralWidget(cw)
    l = QtGui.QVBoxLayout()
    cw.setLayout(l)

    pg.setConfigOption('background', 'w')
    pw = pg.PlotWidget(name='LLH')
    pw.addLegend()
    pw.showGrid(x=True, y=True)
    pw.setXRange(-122.3068, -122.3058)
    l.addWidget(pw)

    c = SpooferConsole(text=console_text, spoofer=spoofer)
    l.addWidget(c)

    mw.show()

    curves['ref_gps_rx'] = pw.plot(pen=pg.mkPen('k', width=2, style=QtCore.Qt.DashLine), name='ref_gps_rx',
                                   symbolBrush=(126, 47, 142), symbolSize=5, symbolPen='k', symbol='o')
    curves['spoofed'] = pw.plot(pen=pg.mkPen('r', width=2, style=QtCore.Qt.DotLine), name='spoofed',
                                symbolBrush=(126, 47, 142), symbolSize=5, symbolPen='r', symbol='+')
    curves['authentic'] = pw.plot(pen=pg.mkPen('b', width=2), name='authentic',
                                  symbolBrush=(126, 47, 142), symbolSize=5, symbolPen='b', symbol='x')


    timer = QtCore.QTimer()
    timer.timeout.connect(lambda: refresh(x_data, y_data, curves))
    timer.start(10)  # in msec
    app.instance().exec_()

# UDP Server
# https://docs.python.org/3.6/library/socketserver.html
# https://gist.github.com/arthurafarias/7258a2b83433dfda013f1954aaecd50a
class ThreadedUDPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        #print("{} wrote:".format(self.client_address[0]))
        self.server.process_udp_packet(data.decode())


class UDPServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
    def __init__(self, server_address, RequestHandlerClass, process_udp_packet):
        self.process_udp_packet = process_udp_packet
        super().__init__(server_address, RequestHandlerClass)


class SpooferConsole(pyqtgraph.console.ConsoleWidget):
    def __init__(self, parent=None, namespace=None, historyFile=None, text=None, editor=None, spoofer=None):
        self.spoofer = spoofer
        super().__init__(parent, namespace, historyFile, text, editor)


    def execSingle(self, cmd):
        try:
            #output = eval(cmd, self.globals(), self.locals())
            output = self.spoofer.process_console_cmd(cmd)
            self.write(repr(output)) # + '\n')
        except SyntaxError:
            try:
                exec(cmd, self.globals(), self.locals())
            except SyntaxError as exc:
                if 'unexpected EOF' in exc.msg:
                    self.multiline = cmd
                else:
                    self.displayException()
            except:
                self.displayException()
        except:
            self.displayException()


class Spoofer:
    def __init__(self):
        self.server = None
        self.sock = None   # For transmitting UDP packets
        self.past_trajectory = []   # Location history of the victim
        self.pred_trajectory = []   # Predicted trajectory of the victim
        self.deviation = []
        self.noise = None
        self.signal = None
        self.gain = None
        self.state = None
        self.loc_update_interval_sec = None
        self.loc_update_thread = None
        self.cmd_update_thread = None
        self.is_running = False
        self.start_time = None
        self.spoofer_usrp_cmds = []
        self.decimal_resolution = 8  # Round up to 6-th decimal point

    def start(self):
        self.start_time = time.time()
        self.server = UDPServer(SPOOFER_CONSOLE_ADDR, ThreadedUDPRequestHandler, self.process_udp_packet)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print("Spoofer console started at {}".format(SPOOFER_CONSOLE_ADDR))

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.is_running = True
        self.loc_update_thread = threading.Thread(target=self.send_loc_update)
        self.loc_update_interval_sec = 1  # in sec; the actual interval is slightly larger than 1 sec
        self.loc_update_thread.start()

        self.cmd_update_thread = threading.Thread(target=self.send_cmd_update)
        self.cmd_update_interval_sec = 0.1  # 1 sec
        self.cmd_update_thread.start()

        self.standby()

    def process_console_cmd(self, cmd):
        cmd = cmd.strip()       # Remove whitespaces
        msg = cmd.split(' ')

        if len(msg) == 2:
            target = msg[0]
            value = msg[1]

            if target == 's':
                self.set_signal_power(value)
                output = 'Setting spoofing signal power = {}'.format(value)
            elif target == 'g':
                self.set_gain(value)
                output = 'Setting hardware gain = {}'.format(value)
            elif target == 'n':
                self.set_noise_power(value)
                output = 'Setting noise power = {}'.format(value)


        elif cmd == 'a':  # STATE_STANDBY -> STATE_TAKEOVER
            if self.state == STATE_STANDBY:
                if (len(self.past_trajectory)) == 0:
                    output = 'Failed: Cannot predict trajectory - no past trajectory.'
                else:
                    output = 'TAKEOVER: starting taking over the GPS lock ...'
                    self.takeover()
            else:
                output = '[{}] Failed to launch GPS takeover ...'.format(self.state)
        elif cmd == 'n':
            if self.state == STATE_TAKEOVER:
                output = 'SPOOFING: setting spoofing noise = 0, and hardware gain = 0'
                self.spoof()
            else:
                output = "[{}] Failed to launch GPS spoofing ...".format(self.state)

        elif cmd == 'norm':
            output = "Setting noise = 0, and gain = 0"
            self.spoofer_usrp_cmds.append((0, self.set_noise_power, 0))
            self.spoofer_usrp_cmds.append((0, self.set_gain, 0))


        elif cmd == 'd':
            if self.state == STATE_SPOOF:
                output = "DEVIATE: deviating victim's trajectory ..."
                self.deviate()

            else:
                output = "[{}] Failed to deviate the victim's trajectory ...".format(self.state)
        elif cmd == 'r':
            output = 'RESET: setting all signal parameters back to default values ...'
            self.reset()

        elif cmd == 'ls':
            output = self.list_params()
            self.send("{},{},{}".format('ls', 0.0, 0.0), SPOOFER_USRP_ADDR)
        else:
            output = 'Unknown command: {}'.format(cmd)

        return output


    def process_udp_packet(self, msg):
        msg  = [x.strip() for x in msg.split(",")]  # Split by comma and remove whitespaces
        label = msg[0]

        if label == 'authentic':
            lat = round(float(msg[2]), self.decimal_resolution)
            lon = round(float(msg[3]), self.decimal_resolution)
            add_data_point(label, lat, lon)
            self.send("{},{},{}".format(label, lat, lon), SIMULATOR_MAPPING_ADDR)

            # ====================== Testing ============================
            if (self.state == STATE_TAKEOVER) | (self.state == STATE_SPOOF):
                hgt = round(float(msg[4]),2)
                msg = 'spoofed {} {} {} {}'.format(self.get_elapsed_time(), lat, lon, hgt)
                self.send(msg, SPOOFER_ENGINE_ADDR)
                print("[{}][LocUpdate] dst={}, msg='{}'".format(self.state, SPOOFER_ENGINE_ADDR, msg))
                add_data_point('spoofed', lat, lon)
                self.send("{},{},{}".format('spoofed', lat, lon), SIMULATOR_MAPPING_ADDR)

            elif self.state == STATE_DEVIATE:
                hgt = round(float(msg[4]), 2)

                lat_dev = self.deviation[0][1]
                lon_dev = self.deviation[0][2]
                hgt_dev = self.deviation[0][3]

                msg = 'spoofed {} {} {} {}'.format(self.get_elapsed_time(), lat + lat_dev, lon + lon_dev, hgt + hgt_dev)
                self.send(msg, SPOOFER_ENGINE_ADDR)
                print("[{}][LocUpdate] dst={}, msg='{}'".format(self.state, SPOOFER_ENGINE_ADDR, msg))
                del self.deviation[0]
                add_data_point('spoofed', lat + lat_dev, lon + lon_dev)
                self.send("{},{},{}".format('spoofed', lat + lat_dev, lon + lon_dev), SIMULATOR_MAPPING_ADDR)

        if label == 'ref_gps_rx':
            #timestamp = float(msg[1])
            lat = round(float(msg[2]), self.decimal_resolution)
            lon = round(float(msg[3]), self.decimal_resolution)
            hgt = float(msg[4])
            fix = int(msg[5])
            elapsed_time = float(msg[6])
            add_data_point(label, lat, lon)
            self.send("{},{},{}".format(label, lat, lon), SIMULATOR_MAPPING_ADDR)

            if fix == 3:
                self.past_trajectory.append((elapsed_time, lat, lon, hgt))

    def send(self, msg, addr):
        self.sock.sendto(bytes(msg, "utf-8"), addr)

    def predict_trajectory(self, interval_sec=1, time_window_sec=3000):
        print('Predicting future trajectory for {} sec'.format(time_window_sec))
        curr_pos = self.past_trajectory[-1][1:3]
        hgt = self.past_trajectory[-1][3]
        speed_km_sec = SPEED_KM_PER_SEC
        bearing_deg = 0

        self.pred_trajectory = []
        for i in range(time_window_sec):
            next_pos = compute_destination(curr_pos, speed_km_sec*interval_sec, bearing_deg)
            self.pred_trajectory.append((0.0, next_pos[0], next_pos[1], hgt))
            #print('Next pos = {}'.format(next_pos))
            curr_pos = next_pos

    def generate_deviation(self):
        lon_delta = DEVIATION_VAL
        self.deviation = []
        for i in range(len(self.pred_trajectory)):
            self.deviation.append((0.0, 0.0, lon_delta*(i**2), 0.0))

    def send_loc_update(self):
        while self.is_running:
            if self.state == STATE_STANDBY:
                if len(self.past_trajectory) > 0:
                    lat = self.past_trajectory[-1][1]
                    lon = self.past_trajectory[-1][2]
                    hgt = self.past_trajectory[-1][3]
                    msg = 'spoofed {} {} {} {}'.format(self.get_elapsed_time(), lat, lon, hgt)
                    self.send(msg, SPOOFER_ENGINE_ADDR)
                    print('[{}][LocUpdate] dst={}, msg={}'.format(self.state, SPOOFER_ENGINE_ADDR, msg))
                    add_data_point('spoofed', lat, lon)
                    self.send("{},{},{}".format('spoofed', lat, lon), SIMULATOR_MAPPING_ADDR)

                else:
                    print('[{}] No location update'.format(self.state))

            # if (self.state == STATE_TAKEOVER) | (self.state == STATE_SPOOF):
            #     if len(self.pred_trajectory) > 0:
            #         lat = self.pred_trajectory[0][1]
            #         lon = self.pred_trajectory[0][2]
            #         hgt = self.pred_trajectory[0][3]
            #
            #         msg = 'spoofed {} {} {} {}'.format(self.get_elapsed_time(), lat, lon, hgt)
            #         self.send(msg, SPOOFER_ENGINE_ADDR)
            #         print("[{}][LocUpdate] dst={}, msg='{}'".format(self.state, SPOOFER_ENGINE_ADDR, msg))
            #         del self.pred_trajectory[0]
            #         add_data_point('spoofed', lat, lon)
            #         self.send("{},{},{}".format('spoofed', lat, lon), SIMULATOR_MAPPING_ADDR)
            #
            #     else:
            #         print('[{}] No predicted location'.format(self.state))
            #
            # if self.state == STATE_DEVIATE:
            #     if len(self.pred_trajectory) > 0:
            #         lat = self.pred_trajectory[0][1]
            #         lon = self.pred_trajectory[0][2]
            #         hgt = self.pred_trajectory[0][3]
            #
            #         lat_dev = self.deviation[0][1]
            #         lon_dev = self.deviation[0][2]
            #         hgt_dev = self.deviation[0][3]
            #
            #         msg = 'spoofed {} {} {} {}'.format(self.get_elapsed_time(), lat+lat_dev, lon+lon_dev, hgt+hgt_dev)
            #         self.send(msg, SPOOFER_ENGINE_ADDR)
            #         print("[{}][LocUpdate] dst={}, msg='{}'".format(self.state, SPOOFER_ENGINE_ADDR, msg))
            #         del self.pred_trajectory[0]
            #         del self.deviation[0]
            #         add_data_point('spoofed', lat+lat_dev, lon+lon_dev)
            #         self.send("{},{},{}".format('spoofed', lat+lat_dev, lon+lon_dev), SIMULATOR_MAPPING_ADDR)
            #
            #     else:
            #         print('[{}] No predicted location'.format(self.state))

            time.sleep(self.loc_update_interval_sec)

    def send_cmd_update(self):
        while self.is_running:
            if len(self.spoofer_usrp_cmds) > 0:
                sleep_time = self.spoofer_usrp_cmds[0][0]
                callback = self.spoofer_usrp_cmds[0][1]
                value = self.spoofer_usrp_cmds[0][2]
                time.sleep(sleep_time)
                callback(value)
                del self.spoofer_usrp_cmds[0]
            else:
                time.sleep(self.cmd_update_interval_sec)

    def set_noise_power(self, value):
        self.noise = value
        self.send('n,{},0.0'.format(value), SPOOFER_USRP_MAIN_ADDR)

    # Spoofer signal power: ON[1] / OFF[0]
    def set_signal_power(self, value):
        self.signal = value
        self.send('s,{},0.0'.format(value), SPOOFER_USRP_MAIN_ADDR)

    def set_gain(self, value, elapsed_time=0.0):
        self.gain = value
        self.send('g,{},0.0'.format(value), SPOOFER_USRP_MAIN_ADDR)

    def get_elapsed_time(self):
        return round(time.time() - self.start_time, 3)

    def standby(self):
        self.noise = 0
        self.signal = 0
        self.gain = 0
        self.state = STATE_STANDBY

    # STATE_STANDBY -> STATE_TAKEOVER
    def takeover(self):
        interval_sec = 0.3
        self.spoofer_usrp_cmds.append((0.0, self.set_noise_power, 2000))
        self.spoofer_usrp_cmds.append((0.1, self.set_gain, 15))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 20))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 25))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 30))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 35))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 40))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 42))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_gain, 45))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_noise_power, 100))
        self.spoofer_usrp_cmds.append((interval_sec, self.set_signal_power, 1))
        self.predict_trajectory()
        self.state = STATE_TAKEOVER

    # STATE_TAKEOVER -> STATE_SPOOF
    # Only transmitting spoofing signal w/o noise
    def spoof(self):
        # self.spoofer_usrp_cmds.append((0, self.set_noise_power, 0))
        # self.spoofer_usrp_cmds.append((0, self.set_gain, 0))
        self.state = STATE_SPOOF

    # STATE_SPOOF -> STATE_DEVIATE
    def deviate(self):
        self.generate_deviation()
        self.state = STATE_DEVIATE

    # STATE_* -> STATE_STANDBY
    def reset(self):
        self.spoofer_usrp_cmds.append((0, self.set_noise_power, 0))
        self.spoofer_usrp_cmds.append((0, self.set_signal_power, 0))
        self.spoofer_usrp_cmds.append((0, self.set_gain, 0))
        self.past_trajectory = []
        self.pred_trajectory = []
        self.deviation = []
        self.state = STATE_STANDBY


    def list_params(self):
        return 'state={}, signal={}, noise={}, gain={}, len(pred_trajectory)={}, len(deviation)={}'.format(
            self.state, self.signal, self.noise, self.gain, len(self.pred_trajectory), len(self.deviation)
        )

    def shutdown(self):
        self.is_running = False
        self.cmd_update_thread.join()
        self.cmd_update_thread.join()


if __name__ == '__main__':
    spoofer = Spoofer()
    spoofer.start()
    display_console(spoofer)

    print('Shutting down ... Waiting for threads to join ...')
    spoofer.shutdown()
